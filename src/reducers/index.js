import { combineReducers } from 'redux';
import fuelSavings from './fuelSavings';

const rootReducer = combineReducers({
  fuelSavings
});

export default rootReducer;
